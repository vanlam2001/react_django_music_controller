"""Cấu hình URL cho dự án music_controller.

Danh sách urlpatterns định tuyến các URL tới các views. Để biết thêm thông tin, vui lòng xem tại:
https://docs.djangoproject.com/en/4.2/topics/http/urls/

Ví dụ:
Views dựa trên hàm
1. Thêm import: from my_app import views
2. Thêm URL vào urlpatterns: path('', views.home, name='home')

Views dựa trên lớp
1. Thêm import: from other_app.views import Home
2. Thêm URL vào urlpatterns: path('', Home.as_view(), name='home')

Bao gồm một URLconf khác
1. Import hàm include(): from django.urls import include, path
2. Thêm URL vào urlpatterns: path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include('api.urls'))
]


